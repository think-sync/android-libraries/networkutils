package com.thinknsync.networkutils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.appcompat.app.AlertDialog;

import com.thinknsync.objectwrappers.AndroidContextWrapper;

public class NetworkUtil implements NetworkStatusReporter {

    private ConnectivityManager cm;
    private Context context;

    public NetworkUtil(AndroidContextWrapper contextWrapper){
        this.context = contextWrapper.getFrameworkObject();
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public int getConnectivityStatus() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }

        return TYPE_NOT_CONNECTED;
    }

    @Override
    public boolean isConnected(boolean showAlert){
        int connectivityStatus = getConnectivityStatus();
        boolean result = connectivityStatus != TYPE_NOT_CONNECTED;
        if(!result && showAlert){
            showNoInternetConnectionMessage();
        }
        return result;
    }

    @Override
    public String getConnectivityStatusString() {
        int statusType = getConnectivityStatus();
        return getConnectivityString(statusType);
    }

    private String getConnectivityString(int medium){
        if (medium == TYPE_WIFI) {
            return "Internet connection available in wifi";
        } else if (medium == TYPE_MOBILE) {
            return "Internet connection available in data";
        } else {
            return "No internet connection. Please connect to internet and try again";
        }
    }

    @Override
    public boolean isMediumConnected(int medium){
        int connectivityStatus = getConnectivityStatus();
        return connectivityStatus == medium;
    }

    private void showNoInternetConnectionMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("")
                .setMessage(getConnectivityString(TYPE_NOT_CONNECTED))
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}