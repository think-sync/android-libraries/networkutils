package com.thinknsync.networkutils;

public interface NetworkStatusReporter {
    int TYPE_NOT_CONNECTED = 0;
    int TYPE_WIFI = 1;
    int TYPE_MOBILE = 2;

    int getConnectivityStatus();
    boolean isConnected(boolean showAlert);
    String getConnectivityStatusString();
    boolean isMediumConnected(int medium);
}
